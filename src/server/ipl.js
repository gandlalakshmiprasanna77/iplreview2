const matchesPlayed = (matches) =>{
    const result = matches.reduce((matchesplayed,value) => {
        matchesplayed[value.season] = (matchesplayed[value.season] || 0)+1;
        return matchesplayed;
    },{});
    return result;
};
const matchesWon = (matches) => {
    const obj1 = matches.reduce((matcheswon,value) => {
        matcheswon[value.season] = (matcheswon[value.season] || {});
        matcheswon[value.season][value.winner] = (matcheswon[value.season][value.winner] || 0) + 1;
        return matcheswon;
    },{});
    return obj1;
};
const extraRuns = (deliveries,matches,year) =>{
    const ids = matches.reduce((array,value) => {
        if (parseInt(value.season) === 2016){
            array.push(value.id);
            return array;
        }
        else{
            return array;
        }
     },[]);
    const deliveriess = deliveries.filter(value => { return ids.includes(value.match_id)});
    const obj3 =  deliveriess.reduce((extraruns,value) => {
         extraruns[value.bowling_team] = (extraruns[value.bowling_team] || 0) + parseInt(value.extra_runs);
         return extraruns;
    },{});
    return obj3;
};
const ecoBowlers = (deliveries, matches) => {
    const dict1 = {}, dict2 = {}, dict3 = {}, dict4 = {};
    matches.filter(obj => obj.season === '2015')
    .forEach((obj) => {
      dict1[obj.id] = 1;
    });
    deliveries.filter((obj) => obj.match_id in dict1).forEach((obj) => {
      if(dict2[obj.bowler] === undefined) {
        dict2[obj.bowler] = {};
      }
      dict2[obj.bowler].runs = (dict2[obj.bowler].runs || 0) + parseInt(obj.total_runs) - parseInt(obj.bye_runs) - parseInt(obj.legbye_runs); 
      if (!parseInt(obj.wide_runs) && !parseInt(obj.noball_runs)) {
        dict2[obj.bowler].overs = (dict2[obj.bowler].overs || 0) + (1 / 6); 
      }
    });
    for (let bowler in dict2) {
      if (dict2[bowler].overs >= 2) {
        dict3[bowler] = dict2[bowler].runs / dict2[bowler].overs;
      }
    }
    Object.entries(dict3)
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10)
    .forEach((arr) => {
      return dict4[arr[0]] = parseFloat(arr[1].toFixed(2));
    });
    return dict4;
  };
const viratseasonalruns =(deliveries,matches,name) => {
    const viratdata = deliveries.filter(value => { return value.batsman === 'V Kohli'});
    const ids = viratdata.map(viratdata => {
        return viratdata.match_id;
    })
    const seasonalids = matches.reduce((seasondata,match) => {
        if(ids.includes(match.id)){
            seasondata[match.season] = (seasondata[match.season] || []);
            seasondata[match.season].push(match.id);
            return seasondata;
        }
        else {
            return seasondata;
        }
    },{});
    const finalarray =[]
    const seasons = Object.keys(seasonalids);
    seasons.forEach((values) => {
        const matchids = seasonalids[values];
        const score = viratdata.reduce((finalscore,value) => {
            if(matchids.includes(value.match_id)){
                finalscore[values] = (parseInt(finalscore[values]) || 0) + parseInt(value.total_runs);
                return finalscore;
            }
            else{
                return finalscore;
            }
        },{});finalarray.push(score);
    });
    return finalarray;
};
module.exports = {
    matchesPlayed,
    matchesWon,
    extraRuns,
    ecoBowlers,
    viratseasonalruns
}